FROM ubuntu:18.04
MAINTAINER Jun FURUSE <jun.furuse@dailambda.jp>

RUN apt-get clean

# Replacing the source

RUN sed -e 's|/archive\.ubuntu\.com/ubuntu|/ftp.jaist.ac.jp/pub/Linux/ubuntu|g' /etc/apt/sources.list > /tmp/sources.list
RUN mv /tmp/sources.list /etc/apt/sources.list
RUN apt-get update

# Basic tools

RUN apt-get install -y --no-install-recommends curl git ca-certificates make patch sudo

# User tezzy

RUN useradd tezzy -m
RUN usermod -aG sudo tezzy
# tezzy can use sudo without password
RUN echo 'tezzy ALL=NOPASSWD: ALL' >> /etc/sudoers
RUN chown -R tezzy /usr/local

# OCaml and OPAM

RUN apt-get install -y --no-install-recommends ocaml
RUN apt-get install -y --no-install-recommends g++
RUN apt install -y --no-install-recommends pkg-config libgmp-dev libev-dev libhidapi-dev
USER tezzy
WORKDIR /home/tezzy
RUN git clone https://github.com/ocaml/opam
WORKDIR opam
RUN git checkout 2.0.3
RUN ./configure
RUN make lib-ext && make && make install
RUN sudo apt install -y --no-install-recommends m4 unzip bubblewrap
RUN opam init -a --bare --disable-sandboxing # bwrap does not work...

# Tezos

WORKDIR /home/tezzy
RUN git clone https://gitlab.com/tezos/tezos
WORKDIR tezos
RUN git checkout alphanet
RUN make build-deps
RUN eval $(opam env); make
WORKDIR /home/tezzy

# Cleanup

WORKDIR /home/tezzy
RUN du -sh /usr /home \
    && sudo apt remove -y ocaml \
    && sudo apt autoremove -y \
    && sudo apt-get clean \
    && rm -rf /home/tezzy/opam \
    && (cd /home/tezzy/tezos; opam clean) \
    && (cd /home/tezzy/tezos/_opam/lib; find . -iname '*.cmt*' | xargs rm) \
    && (cd /home/tezzy/tezos/_build; find . -iname '*.cmt*' | xargs rm) \
    && du -sh /usr /home 

# Run

CMD [ "/bin/bash", "--login" ]
